using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable 

    {
        private AudioSource enemyAudio;
        public event Action OnExploded;

        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
            
        }
        private void Awake()
        {
            DontDestroyOnLoad(this);
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
            enemyAudio = GetComponent<AudioSource>();
            
        }

        public override void Fire()
        {
            // TODO: Implement this later
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
        }
    }
}