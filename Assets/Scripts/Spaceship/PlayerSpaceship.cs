using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        private void Awake()
        {
            DontDestroyOnLoad(this);
        }
        

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
            
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
            
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
            SceneManager.LoadScene("Scene1");
        }
    }
}