using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;

public class GameManager2 : MonoBehaviour
{
    
    [SerializeField] private Enemybossspace enemyBossspace;
    [SerializeField] private int enemyBossspaceHp;
    [SerializeField] private int enemyBossspaceMoveSpeed;
    
    

    void Awake()
    {
        Debug.Assert(enemyBossspace != null, "enemySpaceship cannot be null");
        Debug.Assert(enemyBossspaceHp > 0, "bossSpaceshipHp has to be more than zero");
        Debug.Assert(enemyBossspaceMoveSpeed > 0, "bossSpaceshipMoveSpeed has to be more than zero");
        
        
    }
    

    void Start()
    {
        SpawnBossSpaceship();
    }

    private void SpawnBossSpaceship()
    {
        var spawnedBossShip = Instantiate(enemyBossspace);
        spawnedBossShip.Init(enemyBossspaceHp, enemyBossspaceMoveSpeed);
        spawnedBossShip.OnExploded += OnBossSpaceshipExploded;
    }

    private void OnBossSpaceshipExploded()
    {
        SpawnBossSpaceship();
    }
    
}
