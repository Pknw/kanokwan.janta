using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public AudioSource MySound;

    public void playSound()
    {
        MySound.Play();
    }

    public void stopSound()
    {
        MySound.Stop();
    }
}
